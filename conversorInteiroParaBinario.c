#include <stdio.h>

int main(void)
{
    int numero, resto, binario = 0, count = 1;
    
    printf("Insira um numero inteiro para converter em binario");
    scanf("%d", &numero);

    while(numero > 1)
    {
        resto = numero % 2;
        numero = (numero - resto)/2;
        binario += count * resto;
        count *= 10;
    }

    binario += count * 1;
    printf("%d\n", binario);
    
}