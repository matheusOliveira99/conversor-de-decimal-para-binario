![pipeline status](https://gitlab.com/matheusOliveira99/conversor-de-decimal-para-binario/badges/master/pipeline.svg)

# Conversor de decimal para binário

## **Como funciona a conversão**

Um numero é dividido por 2 e armazenado o resto, o quociente da divisão é dividido por 2, sempre guardando o resto até que ele seja 1. Após isso, pegamos os restos de trás para frente e juntamos.

**Observe o exemplo abaixo:**

![Conversão](IMGS/algoritimo.jpg)

---

## **Implementação em C**
---

### Declaração de variaveis
```c
    int numero, resto, binario = 0, count = 1;
```

---

### Entrada de dados
```c
printf("\n Insira um numero inteiro para converter em binario");
scanf("%d", &numero);
```
- Usamos a função printf() para imprimir na tela uma mensagem mostrando o que o usuário deve digitar;
- depois colocamos um scanf() para gravar o valor digitado na variável numero;

---

### Processamento de dados

#### Entrando no While( numero > 1)

```c
    while(numero > 1)
    {
        resto = numero % 2;
        numero = (numero - resto)/2;
        binario += count * resto;
        count *= 10;
    }
```
- O **while** verifica se o **numero** é maior que 1;

- Caso sim, guarda o **resto** da divisão do **numero** por 2;

- Guarda o quociente dentro do **numero**;

- Multiplica o **contador** pelo resto e soma ao binário e guarda no binário;

- Multiplica o **contador** por 10 e guarda nele mesmo(assim garante que os valores serão registrados da direita para esquerda), por exemplo:

    |**COUNT**|**RESTO**|**COUNT x RESTO**| **BINÁRIO**|
    |---------|---------|-----------------|------------|
    |   1     |    1    |        1        |     1      |
    |   10    |    0    |        0        |     1      |
    |   100   |    0    |        0        |     1      |
    |   1000  |    1    |      1000       |    1001    |
    |   10000 |    1    |      10000      |  **11001** |

---
#### Saindo do While( numero = 1 )

```c
binario += count * 1;
``` 
- Implementa o cociente da ultima divisão, pois seria 1.
---
## Saida de dados:

```c
printf("%d\n", binario);
```
* Imprime o valor em binário;

## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279860/avatar.png?width=400)  | Denilson Amancio | denilson.amanciof | [denilson.amanciof@gmail.com](mailto:denilson.amanciof@gmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8332599/avatar.png?width=400)  | Otávio Bender | otavio.bender | [otaviobender@aluno.utfpr.edu.br](mailto:otaviobender@aluno.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/8279834/avatar.png?width=400)  | Matheus Oliveira | matheusoliveira99 | [matheuso.2020@alunos.utfpr.edu.br](mailto:matheuso.2020@alunos.utfpr.edu.br)
